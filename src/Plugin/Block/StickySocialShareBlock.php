<?php

namespace Drupal\sticky_social_share\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Sticky Social Share' Block.
 *
 * @Block(
 *   id = "sticky_social_share",
 *   admin_label = @Translation("Sticky Social Share Block"),
 *   category = @Translation("sticky social share"),
 * )
 */
class StickySocialShareBlock extends BlockBase
{

    /**
     * {@inheritdoc}
     */
    public function build()
    {

        // Load the configuration from the form.
        $config = $this->getConfiguration();

        $whatsapp = $config['whatsapp'] ? $config['whatsapp'] : '';
        $facebook = $config['facebook'] ? $config['facebook'] : '';
        $google_plus = $config['google_plus'] ? $config['google_plus'] : '';
        $linkedIn = $config['linkedIn'] ? $config['linkedIn'] : '';
        $twitter = $config['twitter'] ? $config['twitter'] : '';
        $pinterest = $config['pinterest'] ? $config['pinterest'] : '';
        $instagram = $config['instagram'] ? $config['instagram'] : '';
        $mail = $config['mail'] ? $config['mail'] : '';
        $icons = $config['place'] ? $config['place'] : '';
        $count = $config['count'] ? $config['count'] : '';
        $target = $config['target'] ? $config['target'] : '';
        $hover = $config['hover'] ? $config['hover'] : '';

        $social_values = [
            'whatsapp' => $whatsapp,
            'facebook' => $facebook,
            'google_plus' => $google_plus,
            'linkedIn' => $linkedIn,
            'twitter' => $twitter,
            'pinterest' => $pinterest,
            'instagram' => $instagram,
            'mail' => $mail,
            'icons' => $icons,
            'count' => $count,
        ];

        $module_handler = \Drupal::service('module_handler');
        $whatsapp_icon = '/' . $module_handler->getModule('sticky_social_share')->getPath() . '/icons/whatsapp.svg';
        $facebook_icon = '/' . $module_handler->getModule('sticky_social_share')->getPath() . '/icons/facebook.svg';
        $google_plus_icon = '/' . $module_handler->getModule('sticky_social_share')->getPath() . '/icons/google_plus.svg';
        $linkedIn_icon = '/' . $module_handler->getModule('sticky_social_share')->getPath() . '/icons/linkedin.svg';
        $twitter_icon = '/' . $module_handler->getModule('sticky_social_share')->getPath() . '/icons/twitter.svg';
        $pinterest_icon = '/' . $module_handler->getModule('sticky_social_share')->getPath() . '/icons/pinterest.svg';
        $instagram_icon = '/' . $module_handler->getModule('sticky_social_share')->getPath() . '/icons/instagram.svg';
        $mail_icon = '/' . $module_handler->getModule('sticky_social_share')->getPath() . '/icons/send_mail.svg';

        $social_icons = [
            'whatsapp_icon' => $whatsapp_icon,
            'facebook_icon' => $facebook_icon,
            'google_plus_icon' => $google_plus_icon,
            'linkedIn_icon' => $linkedIn_icon,
            'twitter_icon' => $twitter_icon,
            'pinterest_icon' => $pinterest_icon,
            'instagram_icon' => $instagram_icon,
            'mail_icon' => $mail_icon,
        ];

        if ($node = \Drupal::routeMatch()->getParameter('node')) {
            $title = $node->title->value;
        }
        return [
            '#theme' => 'sticky_social_share_display',
            '#social_values' => $social_values,
            '#social_icons' => $social_icons,
            '#page_title' => $title,
            '#attached' => [
                'library' => ['sticky_social_share/sticky_social_share'],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function blockForm($form, FormStateInterface $form_state)
    {
        $form = parent::blockForm($form, $form_state);

        $form['stickysocial'] = [
            '#type' => 'details',
            '#title' => $this->t('Sticky Social Share'),
            '#collapsible' => true,
            '#open' => true,
            '#description' => '',
        ];

        $form['stickysocial']['whatsapp'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Whatsapp'),
            '#default_value' => isset($this->configuration['whatsapp']) ? $this->configuration['whatsapp'] : '',
        ];

        $form['stickysocial']['facebook'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Facebook'),
            '#default_value' => isset($this->configuration['facebook']) ? $this->configuration['facebook'] : '',
        ];

        $form['stickysocial']['twitter'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Twitter'),
            '#default_value' => isset($this->configuration['twitter']) ? $this->configuration['twitter'] : '',
        ];

        $form['stickysocial']['google_plus'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Google Plus'),
            '#default_value' => isset($this->configuration['google_plus']) ? $this->configuration['google_plus'] : '',
        ];

        $form['stickysocial']['linkedIn'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Linkedin'),
            '#default_value' => isset($this->configuration['linkedIn']) ? $this->configuration['linkedIn'] : '',
        ];

        $form['stickysocial']['mail'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Mail'),
            '#default_value' => isset($this->configuration['mail']) ? $this->configuration['mail'] : '',
        ];

        $form['stickysocial']['instagram'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Instagram'),
            '#default_value' => isset($this->configuration['instagram']) ? $this->configuration['instagram'] : '',
        ];

        $form['stickysocial']['pinterest'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Pinterest'),
            '#default_value' => isset($this->configuration['pinterest']) ? $this->configuration['pinterest'] : '',
        ];

        $form['stickysocial_position'] = [
            '#type' => 'details',
            '#title' => $this->t('Position'),
            '#collapsible' => true,
            '#open' => true,
            '#description' => '',
        ];

        $form['stickysocial_position']['place'] = [
            '#type' => 'radios',
            '#title' => $this->t('Where do you want to display the icons?'),
            '#required' => true,
            '#default_value' => isset($this->configuration['place']) ? $this->configuration['place'] : 4,
            '#options' => [
                1 => $this->t('Top'),
                2 => $this->t('Right'),
                3 => $this->t('Bottom'),
                4 => $this->t('Left'),
            ],
        ];

        $form['stickysocial_position']['count'] = [
            '#title' => $this->t('Count'),
            '#value' => isset($this->configuration['count']) ? $this->configuration['count'] : 0,
            '#type' => 'hidden',
        ];

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function blockValidate($form, FormStateInterface $form_state)
    {
        $values = $form_state->getValues();
        $links = [];
        $links[] = $values['stickysocial']['whatsapp'];
        $links[] = $values['stickysocial']['facebook'];
        $links[] = $values['stickysocial']['twitter'];
        $links[] = $values['stickysocial']['google_plus'];
        $links[] = $values['stickysocial']['linkedIn'];
        $links[] = $values['stickysocial']['mail'];
        $links[] = $values['stickysocial']['instagram'];
        $links[] = $values['stickysocial']['pinterest'];

        $count = 0;
        if ($links) {
            foreach ($links as $link) {
                if (!empty($link)) {
                    $count = $count + 1;
                }
            }
        }

        if ($count < 2) {
            $form_state->setErrorByName('floatingsocialblock', $this->t('At least two fields should be filled.'));
        }
        $this->configuration['count'] = $count;
    }

    /**
     * {@inheritdoc}
     */
    public function blockSubmit($form, FormStateInterface $form_state)
    {
        parent::blockSubmit($form, $form_state);
        $values = $form_state->getValues();
        $this->configuration['whatsapp'] = $values['stickysocial']['whatsapp'];
        $this->configuration['facebook'] = $values['stickysocial']['facebook'];
        $this->configuration['twitter'] = $values['stickysocial']['twitter'];
        $this->configuration['google_plus'] = $values['stickysocial']['google_plus'];
        $this->configuration['linkedIn'] = $values['stickysocial']['linkedIn'];
        $this->configuration['mail'] = $values['stickysocial']['mail'];
        $this->configuration['instagram'] = $values['stickysocial']['instagram'];
        $this->configuration['pinterest'] = $values['stickysocial']['pinterest'];
        $this->configuration['place'] = $values['stickysocial_position']['place'];
    }
}
